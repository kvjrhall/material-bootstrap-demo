import { TestBed } from '@angular/core/testing';
import { DemonstrationService } from '../demonstration-service/demonstration.service';
import { DemonstrationResolverService } from './demonstration-resolver.service';

describe('DemonstrationResolverService', () => {
  let resolver: DemonstrationResolverService;
  let service: DemonstrationService;

  beforeEach(() => {
    service = jasmine.createSpyObj('DemonstrationService', ['getJson']);
    TestBed.configureTestingModule({
      providers: [{ provide: DemonstrationService, useValue: service }],
    });
    resolver = TestBed.inject(DemonstrationResolverService);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
