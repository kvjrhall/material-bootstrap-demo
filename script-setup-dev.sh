#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset


function loginfo {
  echo -e "\e[33m\e[1m[INFO] ($(basename $0)) $1\e[0m"
}

function heading_1 {
	echo
	loginfo "$1"
	echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m"
}

function heading_2 {
	echo
	loginfo "$1"
	echo -e "\e[33m\e[1m[    ] ($(basename $0)) --------------------------------------------------------------------------------\e[0m"
}

function on_fail {
  echo >&2
  echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m" >&2
  echo -e "\e[33m\e[1m[FAIL] ($(basename $0)) $1\e[0m" >&2
  echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m" >&2
  exit 1
}



if [[ ! -d ./.envs/html ]]; then
  heading_1 "Creating python virtual environment"
  python3.9 -m venv ./.envs/html
else
  loginfo "Virtual environment exists"
fi

source ./.envs/html/bin/activate

heading_1 "Package management tools"
pip install --upgrade pip
pip install --upgrade setuptools
pip install --upgrade wheel
pip install --upgrade tox

heading_1 "Development packages"
pip install --upgrade \
    autopep8 \
    coverage \
    flake8 \
    flake8-mypy \
    jupyter \
    mypy \
    nbstripout \
    pycodestyle \
    pylint \
    tox \
    types-requests \
    types_six

heading_1 "Development dependencies"
pip install --upgrade rdflib
pip install --upgrade beautifulsoup4
pip install --upgrade htmlmin
pip install --upgrade html5lib
