Why this project?

1. Most of the Q&A out there for using Material and Bootstrap together either
   link to the material-bootstrap (commercial) product or to StackOverflow posts
   telling users that they'll have to roll their own.

2. Their are a few good tutorials out there, but nothing I could just pull and
   base a project on.

3. I'd like to have an upstream branch/project where I can focus on simply
   updating and maintaining the style overrides needed.

4. While we can get a lot of the solution pretty easily, there are still many
   overrides that must be manually defined
   (see `src/styles/material-to-bootstrap.css`). These kinds of changes occur on
   an as-needed basis, and so they may come from many projects. I'd like there
   to be the possibility that more experienced CSS/Material/Bootstrap developers
   can contribute.

5. I'd like to make the occasional utility that `@ng-bootstrap` may not have,
   but I find myself needing. Wrappers for Bootstrap Javascript may need some
   special handling when used with server-side rendering, and I'd expect them
   to be reusable across projects.

# Steps Followed

This repository will certainly become out-of-date as the angular ecosystem
evolves and its value as an informative resource will diminish. What may be
more useful in the longer term is the steps taken and the rationale applied.
Hopefully the procedure can then (1) be adapted to API changes and (2) help to
debug problems that may still be easy to encounter.

1. Fork the upstream `complete-prerender-demo` project
2. Replace `app/styles.scss` with the version here
3. Introduce the contents of the `app/styles/` folder
4. `ng add @angular/material`
5. `npm install @ng-bootstrap/ng-bootstrap --force`. We do this because angular-
   bootstrap occasionally depends on slightly older versions of angular, and we
   don't want to downgrade so long as things end up playing nicely. This will
   surely come back to haunt us, so we'll need to keep an eye out
6. `npm install bootstrap` and `npm install @types/bootstrap`

# Conclusion

Now, it's unfortunately going to be the case that the copy-and-paste tasks
in steps 2 & 3 will cease to be correct as bootstrap and material's styles
change. If this project isn't kept up to date, future work will need to tweak
those accordingly. Similarly, step 5 above may change depending on the alignment
of `@ng-bootstrap` and `@angular/material` with Angular's release.
